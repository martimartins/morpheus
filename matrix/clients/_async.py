from __future__ import annotations

from matrix.http import AsyncHTTP
from matrix.user import ClientUser
from matrix.enums import PresenceType

from typing import TYPE_CHECKING, Any, Optional
from asyncio import get_event_loop

import aiohttp
import logging
import asyncio

if TYPE_CHECKING:
    from asyncio import AbstractEventLoop

_log = logging.getLogger(__name__)

__all__ = ("AsyncClient",)


class AsyncClient:
    def __init__(
        self,
        homeserver: str,
        loop: AbstractEventLoop = None,
        device_id: Optional[str] = None,
        ssl: Optional[bool] = False,
        proxy: Optional[str] = None,
        proxy_auth: Optional[aiohttp.BasicAuth] = None,
    ) -> None:
        self.homeserver: str = homeserver
        self.loop: AbstractEventLoop = loop or get_event_loop()
        self.http: AsyncHTTP = AsyncHTTP(homeserver, proxy=proxy, proxy_auth=proxy_auth)

        self.device_id: str = device_id
        self.ssl: bool = ssl

        self.user: ClientUser = None

    async def login(
        self, *, username: str, password: str, device_name: Optional[str] = None
    ) -> None:
        _log.info("Logging in using username and password")

        data = await self.http.login(username, password, device_name, self.device_id)
        self.user = ClientUser(data)
        self.http.token = self.user.token

    async def sync_forever(
        self,
        timeout: Optional[int] = None,
        filter: Optional[Any] = None,
        since: Optional[str] = None,
        full_state: Optional[bool] = None,
        sleep_per_sync: Optional[int] = None,
        presence: Optional[PresenceType] = None,
    ):
        token = self.user.token
        sleep_per_sync: float = sleep_per_sync / 1000

        while True:
            try:
                response = await self.http.sync(
                    token, since, timeout, filter, full_state, presence
                )
                print(response)
            finally:
                await asyncio.sleep(sleep_per_sync)
