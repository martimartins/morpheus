from typing import Any, Dict, Any, TypeVar, Union
from .types.user import User

U = TypeVar("U", bound="Union[BaseUser, str]")

__all__ = ("ClientUser",)


class BaseUser:
    __slots__ = ("user_id",)

    def __init__(self, data: User) -> None:
        self._update(data)

    def __eq__(self, user: U) -> bool:
        return self.user_id == user

    def __ne__(self, user: U) -> bool:
        return not self.__eq__(user)

    def __str__(self):
        return f"{self.user_id}"

    def _update(self, data: User):
        self.user_id = data["user_id"]


class ClientUser(BaseUser):
    def __init__(self, data: User) -> None:
        super().__init__(data)
        self._update(data)
        
        self.rooms = Any

    def _update(self, data: User):
        self.token = data["access_token"]
        self.device_id = data["device_id"]
        self.home_server = data["home_server"]
        self.user_id = data["user_id"]
