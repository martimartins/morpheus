from typing import Any, Optional, TypedDict


class UserRegistrationResponse(TypedDict):
    access_token: str
    device_id: str
    home_server: str
    user_id: str


class _BaseServerInformation(TypedDict):
    base_url: str


_WellKnown = TypedDict("_WellKnown", {"m.homeserver": _BaseServerInformation})


class UserLoginResponse(TypedDict):
    access_token: str
    device_id: str
    home_server: str
    user_id: str
    well_known: _WellKnown


class User(UserLoginResponse):
    avatar_url: Optional[str]
    display_name: Optional[str]
