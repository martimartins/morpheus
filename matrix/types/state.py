from typing import List, TypedDict, Generic, TypeVar

T = TypeVar("T")

class _Events(TypedDict[T]):
    content: T
    
class _CustomConfigKey(TypedDict):
    custom_config_key: str

class _AccountData(TypedDict):
    events: List[_Events[_CustomConfigKey]]
    type: str
    
class _PresenceEvents(_Events):
    sender: str
    type: str
    
class _Presence(TypedDict):
    events: List[_Events[T]]

class State(TypedDict):
    account_data: _AccountData
    next_batch: str
    presence: ...