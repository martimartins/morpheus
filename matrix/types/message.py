from typing import TypedDict

class BodyRequest(TypedDict):
    body: str
    msgtype: str