from typing import Any, TypedDict, Dict, Optional


class _OptionalInformation(TypedDict):
    age: int
    prev_content: Dict[Any, Any]
    redacted_because: Dict[Any, Any]
    redacted_because: Dict[Any, Any]
    transaction_id: str
    
class Event(TypedDict):
    content: Dict[Any, Any]
    event_id: str
    origin_server_ts: int
    sender: str
    state_key: str
    type: str
    unsigned: _OptionalInformation
    room_id: str
