from __future__ import annotations

import aiohttp
import requests
import asyncio
import sys
import logging

from typing import (
    Any,
    Callable,
    Dict,
    Optional,
    TypeVar,
    Coroutine,
    Union,
    overload,
    TYPE_CHECKING,
)

from .enums import LoginType, IdentifierTypes, PresenceType, RoomEvents
from .utils import _to_json, _JSON_LOADER as JSON_LOADER, _parse_ratelimit_header
from .errors import Forbidden, NotFound, ServerError, StandardError

try:
    from urllib.parse import urlencode, quote as uriquote
except ModuleNotFoundError:
    from urllib import urlencode, quote as uriquote

if TYPE_CHECKING:
    from .types import user, state, message, event
    from uuid import UUID

T = TypeVar("T")
_FT = Union[str, Dict[Any, Any]]
TXN = TypeVar("TXN", bound="Union[int, UUID]")
Response = Coroutine[Any, Any, T]

_log = logging.getLogger(__name__)


__all__ = ("AsyncHTTPClient",)


class Route:
    BASE = "/_matrix/client/v3"

    def __init__(self, method: str, path: str, **parameters: Dict[str, str]) -> None:
        self.method: str = method
        self.parameters: str = "?{}".format(urlencode(parameters))
        self.path: str = self.BASE + path + self.parameters

    @staticmethod
    def format(url: str, **parameters) -> str:
        return url.format_map(
            {k: uriquote(v) if isinstance(v, str) else v for k, v in parameters.items()}
        )


class BaseHTTP:
    """Represents an Base HTTP client sending HTTP requests to the Matrix API."""

    def __init__(
        self,
        *,
        homeserver: str,
        loop: Optional[asyncio.AbstractEventLoop] = None,
        proxy: Optional[str] = None,
        proxy_auth: Optional[aiohttp.BasicAuth] = None,
    ) -> None:
        self.loop: asyncio.AbstractEventLoop = (
            asyncio.get_event_loop() if loop is None else loop
        )
        self.homeserver: str = homeserver
        self.token: Optional[str] = None

        user_agent = (
            "MatrixBot (martim13artins13@gmail.com) Python/{0[0]}.{0[1]} aiohttp/{1}"
        )
        self.user_agent = user_agent.format(sys.version_info, aiohttp.__version__)

        # Proxy support
        self.proxy: Optional[str] = proxy
        self.proxy_auth: Optional[aiohttp.BasicAuth] = proxy_auth

    def _create_session(self) -> None:
        raise NotImplementedError

    def request(self) -> Response[T]:
        raise NotImplementedError

    def discovery_info(self) -> Response[user._WellKnown]:
        return self.request(Route("GET", "/.well-known/matrix/client"))

    def login_info(self):
        return self.request(Route("GET", "/login"))

    def register(
        self,
        username: str,
        password: Optional[str] = None,
        device_name: Optional[str] = None,
        device_id: Optional[str] = None,
        inhibit_login: Optional[bool] = None,
    ) -> Response[user.UserRegistrationResponse]:
        """Register for an account on this homeserver.

        Parameters
        ----------
        username: :class:`str`
            The basis for the localpart of the desired Matrix ID
            If omitted, the homeserver MUST generate a Matrix ID local part.
        password: Optional[:class:`str`]
            The desired password for the account.
        device_name: Optional[:class:`str`]
            A display name to assign to the newly-created device.
            Ignored if device_id corresponds to a known device.
        device_id: Optional[:class:`str`]
            ID of the client device.
            If this does not correspond to a known client device,
            a new device will be created.
            The server will auto-generate a device_id if this is not specified.
        inhibit_login: Optional[:class:`bool`]
            If true, an access_token and device_id should not be returned,
            therefore preventing an automatic login. Defaults to false.
        """

        r = Route("POST", "/register")
        payload = {
            "auth": {"type": "m.login.dummy"},
            "username": username,
            "password": password,
        }

        if device_name:
            payload["initial_device_display_name"] = device_name

        if device_id:
            payload["device_id"] = device_id

        return self.request(r, json=payload)

    @overload
    def login(
        self,
        username: str,
        password: str,
        device_name: Optional[str] = None,
        device_id: Optional[str] = None,
    ) -> Response[user.UserLoginResponse]:
        ...

    @overload
    def login(
        self,
        username: str,
        token: str,
        device_name: Optional[str] = None,
        device_id: Optional[str] = None,
    ) -> Response[user.UserLoginResponse]:
        ...

    def login(
        self,
        username: str,
        password: Optional[str] = None,
        device_name: Optional[str] = None,
        device_id: Optional[str] = None,
        token: Optional[str] = None,
    ) -> Response[user.UserLoginResponse]:
        if not password and token:
            raise ValueError("Neither a password nor a token was provided")

        self._create_session()
        r = Route("POST", "/login")
        payload = {}
        identifier = {}

        if password:
            if not username.startswith("@") and "@" in username:
                identifier = {
                    "type": IdentifierTypes.thirdparty,
                    "medium": "email",
                    "address": "address",
                }
            else:
                identifier = {
                    "type": IdentifierTypes.user,
                    "user": username,
                }

            payload = {
                "type": LoginType.password,
                "identifier": identifier,
                "password": password,
            }
        else:
            payload = {"type": LoginType.token, "token": token}

        if device_name:
            payload["initial_device_display_name"] = device_name

        if device_id:
            payload["device_id"] = device_id

        return self.request(r, json=payload)

    def logout(self, token: str, all_devices: bool = False) -> Response[None]:
        path: str = "/logout" if not all_devices else "/logout/all"
        r = Route("POST", path, access_token=token)
        return self.request(r, json={})

    def sync(
        self,
        token: str,
        since: Optional[str] = None,
        timeout: int = 30000,
        filter: Optional[_FT] = None,
        full_state: bool = False,
        set_presence: Optional[PresenceType] = None,
    ) -> Response[state.State]:
        r = Route("GET", "/sync", access_token=token)
        payload = {"timeout": timeout}

        if since:
            payload["since"] = since

        if full_state:
            payload["full_state"] = str(full_state).casefold()

        if set_presence:
            payload["set_presence"] = str(set_presence)

        if isinstance(filter, dict):
            payload["filter"] = _to_json(filter)
        elif isinstance(filter, str):
            payload["filter"] = filter

        return self.request(r, json=payload)

    def room_send(
        self,
        token: str,
        room_id: str,
        event_type: RoomEvents,
        txn_id: TXN,
        body: message.BodyRequest,
    ):
        r = Route(
            "PUT",
            Route.format(
                "/rooms/{room_id}/send/{event_type}/{txn_id}",
                room_id=room_id,
                event_type=event_type,
                txn_id=txn_id,
            ),
            access_token=token,
        )
        payload: message.BodyRequest = body
        return self.request(r, json=payload)

    def get_joined_members(self, token: str, room_id: str):
        r = Route(
            "GET",
            Route.format(
                "/rooms/{room_id}/joined_members",
                room_id=room_id,
            ),
            access_token=token,
        )
        return self.request(r)

    def room_get_event(
        self, token: str, room_id: str, event_id: str
    ) -> Response[event.Event]:
        r = Route(
            "GET",
            Route.format(
                "/rooms/{room_id}/event/{event_id}",
                room_id=room_id,
                event_id=event_id,
            ),
        )
        return self.request(r, access_token=token)

    def room_get_state_event(
        self,
        token: str,
        room_id: str,
        event_type: RoomEvents,
        state_key: str = None,
    ):
        r = Route(
            "GET",
            Route.format(
                "/rooms/{room_id}/state/{event_type}/{state_key}",
                room_id=room_id,
                event_type=event_type,
                state_key=state_key,
            ),
            access_token=token,
        )
        return self.request(r)

    def room_send_state(
        self,
        token: str,
        room_id: str,
        event_type: RoomEvents,
        body: message.BodyRequest,
        state_key: str = None,
    ):
        r = Route(
            "PUT",
            Route.format(
                "/rooms/{room_id}/state/{event_type}/{state_key}",
                room_id=room_id,
                event_type=event_type,
                state_key=state_key,
            ),
            access_token=token,
        )
        payload = body
        return self.request(r, json=payload)

    def set_user_typing(
        self,
        token: str,
        room_id: str,
        user_id: str,
        typing: bool,
        timeout: Optional[int] = None,
    ) -> Response[None]:
        r = Route(
            "PUT",
            Route.format(
                "/rooms/{room_id}/typing/{user_id}",
                room_id=room_id,
                user_id=user_id,
            ),
            access_token=token,
        )
        payload = {
            "typing": typing,
        }

        if timeout:
            payload["timeout"] = timeout

        return self.request(r, json=payload)


class AsyncHTTP(BaseHTTP):
    def __init__(
        self,
        homeserver: str,
        proxy: Optional[str] = None,
        proxy_auth: Optional[aiohttp.BasicAuth] = None,
        connector: Optional[aiohttp.BaseConnector] = None,
        loop: Optional[asyncio.AbstractEventLoop] = None,
    ) -> None:
        super().__init__(homeserver=homeserver, proxy=proxy, proxy_auth=proxy_auth)

        self.connector: aiohttp.BaseConnector = connector or aiohttp.TCPConnector(
            limit=0
        )
        self.__session: aiohttp.ClientSession = None

    def _create_session(self):
        self.__session = aiohttp.ClientSession(connector=self.connector)

    async def request(self, route: Route, **kwargs) -> Response[T]:
        homeserver = self.homeserver
        method = route.method
        path = route.path
        url = f"{homeserver}{path}"

        headers = {"User-Agent": self.user_agent}

        if "json" in kwargs:
            headers["Content-Type"] = "application/json"
            kwargs["data"] = _to_json(kwargs.pop("json"))

        kwargs["headers"] = headers

        if self.proxy:
            kwargs["proxy"] = self.proxy
        if self.proxy_auth:
            kwargs["proxy_auth"] = self.proxy_auth

        # 5 tries
        for trie in range(5):
            async with self.__session.request(method, url, **kwargs) as resp:
                _log.debug("%s %s has returned %s", method, url, resp.status)

                data = await resp.json(loads=JSON_LOADER)
                status = resp.status

                if 300 > resp.status >= 200:
                    _log.debug("%s %s has received %s", method, url, data)
                    return data

                # rate limit handle
                if status == 429:
                    retry_after_secs: int = data["retry_after_ms"] / 1000
                    _log.warning(
                        "You are being rate limit, trying again after %.2f seconds",
                        retry_after_secs,
                    )

                    await asyncio.sleep(retry_after_secs)
                    _log.debug("Done sleeping for the rate limit, retrying...")
                    continue

                if status in (500, 502, 504):
                    try_after: int = 1 + trie * 2
                    _log.debug(
                        "Internal error received from %s, trying again after %s seconds",
                        self.homeserver,
                        try_after,
                    )
                    await asyncio.sleep(try_after)

                if status == 403:
                    raise Forbidden(resp, data)
                elif status == 404:
                    raise NotFound(resp, data)
                elif status >= 500:
                    raise ServerError(resp, data)
                else:
                    raise StandardError(resp, data)
