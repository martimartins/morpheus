from aiohttp.web import Response
from typing import TypeVar, Dict

T = TypeVar("T")

class StandardError(Exception):
    """Represents the standart error of matrix

    Any errors which occur at the Matrix API level
    MUST return a "standard error response".
    """

    def __init__(self, response: Response[T], data: Dict[str, str]) -> None:
        self.response = response
        self.errcode: str = data.get("errcode")
        self.error: str = data.get("error")
        super().__init__(self.errcode, self.error)


class Forbidden(StandardError):
    """Forbidden access,
    e.g. joining a room without permission, failed login.

    Subclass of :exc:`StandardError`.

    .. versionadded:: 0.1b
    """

    pass


class NotFound(StandardError):
    """No resource was found for this request.

    Subclass of :exc:`StandardError`.

    .. versionadded:: 0.1b
    """

    pass


class ServerError(StandardError):
    """Exception that's raised for when a 500 range status code occurs.

    Subclass of :exc:`StandardError`.

    .. versionadded:: 0.1b
    """

    pass
