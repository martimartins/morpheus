"""
Matrix Client SDK
~~~~~~~~~~~~~~~~~~~
A basic SDK for matrix chat.

:copyright: (c) 2022-present Martim Martins
:license: MIT, see LICENSE for more details.
"""
__title__ = "morpheus-matrix"
__author__ = "martimmartins <martim13artins13@gmail.com>"
__license__ = "MIT"
__copyright__ = "Copyright 2022-present martimartins"
__version__ = "0.1b"

__path__ = __import__("pkgutil").extend_path(__path__, __name__)