from enum import Enum


class BaseEnum(str, Enum):
    def __str__(self):
        return self.value


class LoginType(BaseEnum):
    password = "m.login.password"
    token = "m.login.token"

    # Aliases
    passwd = "m.login.password"


class IdentifierTypes(BaseEnum):
    user = "m.id.user"
    thirdparty = "m.id.thirdparty"
    phone = "m.id.phone"


class PresenceType(BaseEnum):
    offline = "offline"
    online = "online"
    unavailable = "unavailable"

    # Aliases
    dnd = "unavailable"


class RoomEvents(BaseEnum):
    canonical_alias = "m.room.canonical_alias"
    create = "m.room.create"
    join_rules = "m.room.join_rules"
    member = "m.room.member"
    power_levels = "m.room.power_levels"
    message = "m.room.message"
