from typing import Any, Final, Dict

import json

try:
    import orjson
except ModuleNotFoundError:
    HAS_ORJSON = False
else:
    HAS_ORJSON = True



if HAS_ORJSON:

    def _to_json(obj: Any) -> str:
        return orjson.dumps(obj).decode("utf-8")

    _JSON_LOADER = orjson.loads
else:

    def _to_json(obj: Any) -> str:
        return json.dumps(obj, separators=(",", ":"), ensure_ascii=True)

    _JSON_LOADER = json.loads


class _MissingSentinel:
    def __bool__(self) -> bool:
        return False

    def __int__(self) -> int:
        return 0

    def __eq__(self, __o: object) -> bool:
        return False

    def __repr__(self) -> str:
        return "<MissingSentinel: ...>"

MISSING: Final[_MissingSentinel] = _MissingSentinel()

def _parse_ratelimit_header(headers: Dict[str, str]) -> float:
    retry_after = headers.get("retry_after_ms")
    # TODO: Implementation of sys clock not sync
    return float(retry_after)


