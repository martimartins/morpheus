import asyncio
import logging

from matrix.clients import AsyncClient

#logging.basicConfig(level=logging.NOTSET)

client = AsyncClient("https://matrix.org")


async def main():
    await client.login(username="...", password="...")
    data = await client.http.sync(
        client.user.token
    )
    print("SYNC FEITO COM SUCESSO!")
    with open("sync.txt", "w+", encoding="utf-8") as f:
        f.write(str(data))
    #await client.sync_forever(sleep_per_sync=1000)


if __name__ == "__main__":
    
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
